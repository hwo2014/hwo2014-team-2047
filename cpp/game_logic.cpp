#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

bool g_bTestLap = false;

game_logic::game_logic()
  /*: action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    }*/
{
	action_map.insert(std::make_pair("join", &game_logic::on_join));
	action_map.insert(std::make_pair("gameStart", &game_logic::on_game_start));
	action_map.insert(std::make_pair("carPositions", &game_logic::on_car_positions));
	action_map.insert(std::make_pair("crash", &game_logic::on_crash));
	action_map.insert(std::make_pair("gameEnd", &game_logic::on_game_end));
	action_map.insert(std::make_pair("error", &game_logic::on_error));
	action_map.insert(std::make_pair("gameInit", &game_logic::on_gameInit));
}

int nThrottle = 1;
game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);

  if ( msg_type == "carPositions" )
	  std::cout << msg["gameTick"].as_longlong() << std::endl;
	
   

   if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    //std::cout << "Unknown message type: " << msg_type << std::endl;

	//////////////////////////////////////////////////////////////////////////
// 	std::cout << msg;
// 	std::cout << std::endl;
	//////////////////////////////////////////////////////////////////////////

	game_logic::msg_vector vec;
	vec.push_back(make_ping());
    return vec;
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  game_logic::msg_vector vec;
  vec.push_back(make_ping());
  return vec;
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  game_logic::msg_vector vec;
  vec.push_back(make_ping());
  return vec;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	const auto& myself = data[0];
	const auto& piece = myself["piecePosition"];
	const auto& lap = piece["lap"];
	const auto& pieceIndex = piece["pieceIndex"];

	unsigned long long nPieceIndex = pieceIndex.as_ulonglong();
	unsigned long long nLapIndex = lap.as_ulonglong();


	game_logic::msg_vector vec;

	calculateChangeLaneMessage(nPieceIndex, vec);

  
	if ( nPieceIndex < 33 && nLapIndex == 0 )
		vec.push_back(make_throttle(0.6));
	else if ( nPieceIndex >= 33 || nPieceIndex < 4 )
		 vec.push_back(make_throttle(1));
	else 
		  vec.push_back(make_throttle(0));
	return vec;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;

  game_logic::msg_vector vec;
  vec.push_back(make_ping());
  return vec;
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  game_logic::msg_vector vec;
  vec.push_back(make_ping());
  return vec;
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  game_logic::msg_vector vec;
  vec.push_back(make_ping());
  return vec;
}

game_logic::msg_vector game_logic::on_gameInit(const jsoncons::json& data)
{
	const auto& race = data["race"];
	const auto& track = race["track"];
	const auto& id = track["name"];

	std::cout << "Game Track: " << id.as_string() << std::endl;

// 	for ( auto it = pieces. ; it != pieces.as_vector().end(); ++it ) {
// 		int k = 0;
// 	}


	game_logic::msg_vector vec;
	vec.push_back(make_ping());
	return vec;
}

//////////////////////////////////////////////////////////////////////////

void game_logic::calculateChangeLaneMessage(int nPieceIndex, game_logic::msg_vector& vec)
{
	static int nLastSwitchPieceIndex = -1;
	if ( nLastSwitchPieceIndex == nPieceIndex )
		return;

	nLastSwitchPieceIndex = nPieceIndex;
	switch ( nPieceIndex )  {
		case 2:
			vec.push_back(make_switch(1));
			break;
		case 7:
			vec.push_back(make_switch(-1));
			break;
 		case 12:
 			vec.push_back(make_switch(-1));
 			break;
 		case 17:
		case 24:
		case 28:
		case 34:
 			vec.push_back(make_switch(1));
 			break;
	}
}

game_logic::msg_vector game_logic::on_car_positions_test_Lap(const jsoncons::json& data) 
{
	game_logic::msg_vector vec;

	const auto& myself = data[0];
	const auto& piece = myself["piecePosition"];
	const auto& lap = piece["lap"];
	const auto& pieceIndex = piece["pieceIndex"];

	unsigned long long nPieceIndex = pieceIndex.as_ulonglong();
	unsigned long long nLapIndex = lap.as_ulonglong();
		
	if ( nPieceIndex < 4 ) {
		vec.push_back(make_throttle(1));
	}
	
	return vec;
}

double Phi_0(double dVStart)
{
	double dSum = 0;
	double dDeltaPhi = deltaPhi_0(dVStart);
	double dV = dVStart;

	while ( dDeltaPhi > 0.001 ) {
		dSum += dDeltaPhi;
		dV = v_0(dV);
		dDeltaPhi = deltaPhi_0(dV);
	}

	return dSum;
}

double vMax(double dAlphaMax)
{
	double dVMin = 0, dVMax = 10;
	int nIteration = 0;

	while ( dVMax - dVMin > 0.001 && nIteration < 1000 ) {
		double dV = (dVMax + dVMin) / 2.0;

		if ( Phi_0( dV ) < dAlphaMax )
			dVMin = dV;
		else 
			dVMax = dV;

		nIteration++;
	}

	return (dVMax + dVMin) / 2.0;
}

double breakingLength(double dVStart, double dVEnd)
{
	if ( dVEnd >= dVStart )
		return 0;

	double dV = dVStart;
	double dSum = 0;

	do {
		dV = v_0(dV);
		dSum += dV;
	} while ( dV > dVEnd );

	dSum -= dV;
	dSum += (dVStart + dVEnd) / 2.0;

	return dSum;
}

double throttleLine(double dVStart, double dVMax, double dLengthToCurve)
{
	double dV = v_1(dVStart);

	if ( breakingLength(dV, dVMax) < dLengthToCurve - (dVStart + dV) / 2.0 )
		return 1;
	
	return 0;
}

double throttleCurve(double dVStart, double dAlphaStart, double dLengthToCurveEnd)
{
	double dV = v_1(dVStart);
	double dFullAngle = Phi_0(dV) + dAlphaStart + Phi_1(dVStart);

	if ( fabs(dFullAngle) < dAlphaMax )
		return 1;

	return 0;
}
