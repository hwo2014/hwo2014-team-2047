# ************************************************************
# Sequel Pro SQL dump
# Версия 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: 127.0.0.1 (MySQL 5.6.13)
# Схема: hwo
# Время создания: 2014-04-22 11:33:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы games
# ------------------------------------------------------------

DROP TABLE IF EXISTS `games`;

CREATE TABLE `games` (
  `game_id` varchar(255) NOT NULL DEFAULT '',
  `notes` varchar(4000) DEFAULT '',
  `date_start` datetime NOT NULL,
  `date_finish` datetime DEFAULT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` varchar(255) DEFAULT '',
  `game_tick` int(11) DEFAULT NULL,
  `msg_type` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  PRIMARY KEY (`id`),
  KEY `fk_games_log` (`game_id`),
  CONSTRAINT `fk_games_log` FOREIGN KEY (`game_id`) REFERENCES `games` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы positions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `positions`;

CREATE TABLE `positions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` varchar(255) NOT NULL DEFAULT '',
  `game_tick` int(11) DEFAULT NULL,
  `angle` double(20,10) DEFAULT NULL,
  `piece_index` int(11) NOT NULL,
  `piece_distance` double(20,10) NOT NULL DEFAULT '0.0000000000',
  `lane_start` tinyint(1) NOT NULL DEFAULT '0',
  `lane_end` tinyint(1) NOT NULL DEFAULT '0',
  `lap` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `game_id_index` (`game_id`),
  CONSTRAINT `fk_games_positions` FOREIGN KEY (`game_id`) REFERENCES `games` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы track
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track`;

CREATE TABLE `track` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `start_point_x` int(11) NOT NULL,
  `start_point_y` int(11) NOT NULL,
  `start_point_angle` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы track_lanes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_lanes`;

CREATE TABLE `track_lanes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `track_id` varchar(255) NOT NULL,
  `index` tinyint(1) NOT NULL,
  `distance_from_center` double(20,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы track_pieces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_pieces`;

CREATE TABLE `track_pieces` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `track_id` varchar(255) NOT NULL DEFAULT '',
  `index` int(11) NOT NULL,
  `length` double(20,10) DEFAULT NULL,
  `switch` tinyint(1) NOT NULL DEFAULT '0',
  `radius` double(20,10) DEFAULT NULL,
  `angle` double(20,10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
