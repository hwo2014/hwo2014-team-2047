var net = require("net");


var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var i = 0;
var throttle = 1;
var prevDistance = 0;


var prevSpeed = 0;
var prevThrottle = 0;

var alfa = 0;
var prevAlfa = 0;


var X = [];
var Y = [];

var dA = [];
var dV = [];

var storeManager = require('./models/storeManager.js').storeManager;
var Track = require('./models/track.js').Track;
var Ferrary = require('./bots/ferrari.js');

var car;


console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);


storeManager.connect();

client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});


function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

var track;

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function (data) {

    if (data.msgType === 'join') {
        console.log('Joined');
    }

    if (data.msgType === 'yourCar') {
        storeManager.saveGame(data);
        car = new Ferrary(data.data['name'], data.data['color']);
    }

    if (data.msgType === 'gameInit') {
        storeManager.saveTrack(data);
        track = new Track(data.data);
        car.setTrack(track);
    }

    if (data.msgType === 'gameStart') {
        console.log('Race started');
        car.start();
    }

    if (data.msgType === 'carPositions') {

        storeManager.savePosition(data);

        throttle = car.setPosition(data.data)

    }

    if (data.msgType === 'gameEnd') {
        console.log('Race ended');
    }

    if (data.msgType === 'finish') {
        storeManager.saveFinishTime(data);
        storeManager.disconnect();
    }

    if (data.msgType === 'crash') {
        console.log('Crashed!!');
        //process.exit(1);
        /*for (var lane in track.routes) {
            for (var i = 0, len = track.routes[lane].length; i < len; i++) {
                var route = track.routes[lane][i], vMax;
                vMax = route.vMax();
                console.log("vMax for lane " + lane + " route " + i + " is: " + vMax);
            }
        }*/
    }

    if (throttle != null) {
        send({
            msgType: "throttle",
            data: throttle
        });
    } else {
        send({
            msgType: "ping",
            data: {}
        });
    }

    storeManager.saveLog(data);

});

jsonStream.on('error', function () {
    return console.log("disconnected");
});


