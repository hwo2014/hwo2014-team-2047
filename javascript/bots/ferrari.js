var Bot = require("./bot.js");
var utils = require("../models/utils.js").utils;

var Ferrary = Bot.extend({


    /**
     * START
     * ACCELERATE_S
     * BRAKE_S
     * MOVE
     * ACCELERATE_B
     * BRAKE_B
     *
     */
    state: "ACCELERATE",

    pLane: undefined,
    cLane: undefined,
    nLane: undefined,

    pAngle: 0,
    cAngle: undefined,

    pSpeed: 0,
    cSpeed: undefined,

    pDistance: 0,
    cDistance: undefined,

    cPiece: undefined,

    stateDistance: undefined,


    /**
     * Interpolated function of speed with Throttle = 0
     */
    v0: undefined,

    /**
     * Interpolated function of speed with Throttle = 1
     */
    v1: undefined,

    // Temp
    X: [],
    Y: [],

    V: [],
    deltaA: [],



    start: function() {
        this.state = "START";
    },

    getPositionOnRoute: function() {

    },

    setPosition: function(positions) {

        var throttle = null;

        data = this.getMyCarData(positions);

        this.cPiece = data.piecePosition.pieceIndex;
        this.cDistance = this.track.getPastDistance(data.piecePosition.pieceIndex, data.piecePosition.inPieceDistance, data.piecePosition.lane.startLaneIndex, data.piecePosition.lap);
        this.cSpeed = this.cDistance - this.pDistance;
        this.cAngle = data.angle;

        this.cRoute = this.track.getCurrentRoute(this.cPiece);

        // Learning V1 ---------------------------------------------------------------------------------------------------
        if (this.cRoute.type === "STRAIGHT" && !this.cRoute.v1) {

            if (this.state === "START" || this.state === "MOVE") {
                this.stateFinish = this.cDistance + 300;
                this.X = [];
                this.Y = [];
                console.log("defining v1");
                this.state = "ACCELERATE";
            } if (this.state === "ACCELERATE" && this.cDistance < this.stateFinish) {
                throttle = 1;
                this.X.push(this.pSpeed);
                this.Y.push(this.cSpeed);
            } if (this.state === "ACCELERATE" && this.cDistance > this.stateFinish) {
                var v1 = utils.interpolation(this.X, this.Y);
                this.track.updateVFunction(this.cRoute, v1, 'v1');
                console.log("defined v1");
                this.state = "MOVE";
            }
        }

        // Learning V0 ---------------------------------------------------------------------------------------------------
//        if (this.cRoute.type === "STRAIGHT" && !this.cRoute.v0) {
//
//            if (this.state === "MOVE") {
//                this.stateFinish = this.cDistance + 150;
//                this.X = [];
//                this.Y = [];
//                console.log("defining v0");
//                this.state = "BREAK";
//            } else if (this.state === "BREAK" && this.cDistance < this.stateFinish) {
//                throttle = 0;
//                this.X.push(this.pSpeed);
//                this.Y.push(this.cSpeed);
//            } else if (this.state === "BREAK" && this.cDistance > this.stateFinish) {
//                var v0 = utils.interpolation(this.X, this.Y);
//                this.track.updateVFunction(this.cRoute, v0, 'v0');
//                console.log("defined v0");
//                throttle = 1;
//                this.state = "MOVE";
//            }
//        }

        // Learning alpha1 ---------------------------------------------------------------------------------------------------
//        if (this.cRoute.type === "BEND" && !this.cRoute.alpha1) {
//
//            if (this.state === "MOVE") {
//                this.stateFinish = this.cDistance + 100;
//                this.X = [];
//                this.Y = [];
//                console.log("defining alpha1");
//                this.state = "ACCELERATE_B";
//            } else if (this.state === "ACCELERATE_B" && this.cDistance < this.stateFinish) {
//                throttle = 1;
//                this.X.push(this.cSpeed);
//                this.Y.push(this.cAngle - this.pAngle);
//            } else if (this.state === "ACCELERATE_B" && this.cDistance > this.stateFinish) {
//                var alpha1 = utils.interpolation(this.X, this.Y);
//                this.track.updateRouteInterpolationFunction(this.cRoute, alpha1, 'alpha1');
//                console.log("defined alpha1");
//                this.state = "MOVE";
//            }
//
//        }

        // Learning alpha0 ---------------------------------------------------------------------------------------------------
        if (this.cRoute.type === "BEND" && !this.cRoute.alpha0) {

            if (this.state === "MOVE") {
                this.stateFinish = this.cDistance + 100;
                this.X = [];
                this.Y = [];
                this.state = "BRAKE_B";
                console.log("defining alpha0");
                throttle = 0;
            } else if (this.state === "BRAKE_B" && this.cDistance < this.stateFinish) {

                this.X.push(this.pSpeed);
                this.Y.push(this.cSpeed);

                this.V.push(this.cSpeed)
                this.deltaA.push(this.cAngle - this.pAngle)

            } else if (this.state === "BRAKE_B" && this.cDistance > this.stateFinish) {
                var alpha0 = utils.interpolation(this.V, this.deltaA);
                var v0 = utils.interpolation(this.X, this.Y);
                console.log("defined v0 for 7.5" + v0(7.5));
                this.track.updateVFunction(this.cRoute, v0, 'v0');
                this.track.updateRouteInterpolationFunction(this.cRoute, alpha0, 'alpha0');
                var vMax = this.cRoute.vMax();

                this.track.updateRouteInterpolationFunction(this.cRoute, vMax, 'vMax');
                console.log("defined alpha0");
                console.log(vMax);

                this.state = "MOVE";
                throttle = 1;
            }

        }




        this.pDistance = this.cDistance;
        this.pSpeed = this.cSpeed;
        this.pAngle = this.cAngle;


        return throttle;
    }


})


module.exports = Ferrary;