var HWO = require("../models/utils.js").HWO;

var Bot = HWO.Model.extend({

    /**
     * Track need to be completed
     */
    track: undefined,

    /**
     * Car name
     */
    name: undefined,

    /**
     * Car color
     */
    color: undefined,


    setTrack: function(track) {
        this.track = track;
    },

    initialize: function(name, color) {
        this.name = name;
        this.color = color;
    },

    getMyCarData: function(positions) {
        for (var i = 0; i < positions.length; i++) {
            if (positions[i].id.name === this.name) {
                return positions[i];
            }
        }
    },

    /**
     * Called on game start
     */
    start: function() {

    },

    /**
     * Set current car position
     *
     * Return throttle
     *
     * Should be implemented in childs
     */
    setPosition: function() {}


});


module.exports = Bot;