var HWO = {

}

HWO.Model = {

    extend: function(data) {

        var self = this;

        function temp() {

            for (var key in data) {
                this[key] = data[key];
            }

            if (typeof data.initialize === 'function') {
                data.initialize.apply(this, Array.prototype.slice.call(arguments, 0));
            }

        }


        temp.extend = function(newData) {
            var temp = {};
            for (var key in data) {
                temp[key] = data[key];
            }

            for (var key in newData) {
                temp[key] = newData[key];
            }
            return self.extend(temp);
        }

        return temp;

    }

}

var utils = {

    interpolation: function (X, Y) {

        var XX = [];
        var YY = [];

        var i = 1;
        XX.push(X[0]);
        YY.push(Y[0]);

        while (i <= 10) {
            var j = Math.round((X.length - 1) * i / 10);
            XX.push(X[j]);
            YY.push(Y[j]);
            i++;
        }

        return function (x) {
            var n = XX.length;
            var res = 0;
            for (var i = 0; i < n; i++) {
                var temp = 1;
                for (var j = 0; j < n; j++) {
                    if (i != j) {
                        temp *= (x - XX[j]) / (XX[i] - XX[j]);
                    }
                }
                res += YY[i] * temp;
            }
            return res;
        }
    }

}


module.exports.HWO = HWO;
module.exports.utils = utils;