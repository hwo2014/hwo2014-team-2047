var mysql = require("mysql");

connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'qwerty',
    database: 'hwo'
});


var storeManager = {

    connect: function () {
        connection.connect();
    },

    saveLog: function(data) {
        var row = {
            'msg_type': data.msgType,
            'game_id': data.gameId,
            'game_tick': data.gameTick,
            'data': JSON.stringify(data.data)
        }
        connection.query('INSERT INTO log SET ?', row, function(err, result) {
            if (err) {
                throw err;
            }
        });
    },

    saveGame: function(data) {
        var row = {
            game_id: data.gameId,
            date_start: new Date()
        }
        connection.query('INSERT INTO games SET ?', row, function(err, result) {
            if (err) {
                throw err;
            }
        });
    },

    saveTrack: function(data) {

        var trackId = data.data.race.track.id;

        connection.query('SELECT * FROM track WHERE id = ?', [trackId], function(err, results) {
            if (err) {
                throw err;
            }

            if (results.length <= 0) {

                var row = {
                    id: trackId,
                    name: data.data.race.track.name,
                    start_point_x: data.data.race.track.startingPoint.position.x,
                    start_point_y: data.data.race.track.startingPoint.position.y,
                    start_point_angle: data.data.race.track.startingPoint.angle
                }

                connection.query('INSERT INTO track SET ?', row, function(err, result) {
                    if (err) {
                        throw err;
                    }
                });

                for (var i = 0; i < data.data.race.track.pieces.length; i++) {
                    row = {
                        track_id: trackId,
                        index: i,
                        length: data.data.race.track.pieces[i].length,
                        switch: data.data.race.track.pieces[i].switch?1:0,
                        radius: data.data.race.track.pieces[i].radius,
                        angle: data.data.race.track.pieces[i].angle
                    }
                    connection.query('INSERT INTO track_pieces SET ?', row, function(err, result) {
                        if (err) {
                            throw err;
                        }
                    });
                }

                for (var i = 0; i < data.data.race.track.lanes.length; i++) {
                    row = {
                        track_id: trackId,
                        index: data.data.race.track.lanes[i].index,
                        distance_from_center: data.data.race.track.lanes[i].distanceFromCenter
                    }
                    connection.query('INSERT INTO track_lanes SET ?', row, function(err, result) {
                        if (err) {
                            throw err;
                        }
                    });
                }


            }
        });

    },

    savePosition: function(data) {
        var row = {
            game_id: data.gameId,
            game_tick: data.gameTick,
            angle: data.data[0].angle,
            piece_index: data.data[0].piecePosition.pieceIndex,
            piece_distance: data.data[0].piecePosition.inPieceDistance,
            lane_start: data.data[0].piecePosition.lane.startLaneIndex,
            lane_end: data.data[0].piecePosition.lane.endLaneIndex,
            lap: data.data[0].piecePosition.lap
        }
        connection.query('INSERT INTO positions SET ?', row, function(err, result) {
            if (err) {
                throw err;
            }
        });
    },

    saveFinishTime: function(data) {
        connection.query('UPDATE games SET date_end = ? WHERE game_id = ?', [new Date(), data.gameId], function(err, result) {
            if (err) {
                throw err;
            }
        });
    },

    disconnect: function() {
        connection.end();
    }

}



module.exports.storeManager = storeManager;
