var HWO = require("./utils.js").HWO;
var _ = require('underscore-node');

var Piece = HWO.Model.extend({

    /**
     *  ['straight', 'bend']
     */
    type: undefined,

    index: undefined,

    length: undefined,

    canSwitch: undefined,

    radius: undefined,

    angle: undefined,

    getPieceLength: function(lane) {
        if (this.type === "STRAIGHT") {
            return this.length;
        }

        if (this.type === "BEND") {
            return Math.PI * (-lane.distance_from_center + this.radius) * Math.abs(this.angle) / 180;
        }
    },

    initialize: function(index, length, radius, angle, canSwitch) {
        this.index = index;
        this.type = radius?"BEND":"STRAIGHT";
        this.length = length;
        this.radius = radius;
        this.angle = angle;
        this.canSwitch = canSwitch?true:false;
    }


});



var Lane = HWO.Model.extend({

    index: undefined,

    distance_from_center: undefined,

    initialize: function(index, distance_from_center) {
        this.index = index;
        this.distance_from_center = distance_from_center;
    }

});

var Route = HWO.Model.extend({
    type:  "STRAIGHT", // "STRAIGHT", "BEND"
    lenght: 0,
    angle:  0,
    pieces: [],
    alphaMax: 59,
    alpha0: undefined,
    alpha1: undefined,
    v0: undefined,
    v1: undefined,
    vMaxValue: undefined,
    length: 0,

    vMax: function() {
        var v, vMin = 0, vMax = 10, iteration = 0;

        while ( vMax - vMin > 0.001 && iteration < 1000 ) {
            v = (vMax + vMin) / 2.0;

            if ( this.phi0(v) < this.alphaMax ) {
                vMin = v;
            } else {
                vMax = v;
            }

            iteration++;
        }

        this.vMaxValue = (vMax + vMin) / 2.0;
        return this.vMaxValue;
    },

    phi0: function(vStart) {
        var sum = 0, deltaPhi = this.alpha0(vStart),
            v = vStart;

        while ( deltaPhi > 0.001 ) {
            sum += deltaPhi;
            v = this.v0(v);
            deltaPhi = this.alpha0(v);
        }

        return sum;
    },

    breakingLength: function(vStart, vEnd) {
        if ( vEnd >= vStart ) {
            return 0;
        }

        var v = vStart, sum = 0;

        do {
            v = this.v0(v);
            sum += v;
        } while ( v > vEnd );

        sum -= v;
        sum += (vStart + vEnd) / 2.0;

        return sum;
    },

    throttleLine: function(vStart, lengthToCurve) {
        var v = v1(vStart);

        if ( this.breakingLength(v, this.vMax) < lengthToCurve - (vStart + v) / 2.0 ) {
            return 1;
        }
        
        return 0;
    },

    throttleCurve: function(vStart, alphaStart, lengthToCurveEnd) {
       var v = v1(vStart),
           fullAngle = phi0(v) + alphaStart + this.alpha1(vStart);

        if ( Math.abs(fullAngle) < this.alphaMax ) {
            return 1;
        }

        return 0;
    }
});


var Track = HWO.Model.extend({

    id: undefined,
    name: undefined,

    /**
     * Array of Piece Interface
     */
    pieces: [],

    /**
     * Track lane info
     *
     * Array of Lanes
     */
    lanes: [],

    startingPoint: {},

    /**
     * Parts of track grouped by type for each lane
     *
     * {
     *   type: ["STRAIGHT", "BEND"],
     *   length: float,
     *   angle: float,
     *   pieces: [],
     *
     *   v0: function,
     *   v1: function,
     *
     *   alphaMax: float,
     *   alpha0: function,
     *   alpha1: function,
     * }
     *
     */
    routes: [],

    initialize: function(data) {

        this.id = data.race.track.id;
        this.name = data.race.track.name;
        this.startingPoint = data.race.track.startingPoint;

        for (var i = 0; i < data.race.track.pieces.length; i++) {
            this.pieces[i] = new Piece(i, data.race.track.pieces[i].length, data.race.track.pieces[i].radius, data.race.track.pieces[i].angle, data.race.track.pieces[i].switch);
        }

        for (var i = 0; i < data.race.track.lanes.length; i++) {
            this.lanes[data.race.track.lanes[i].index] = new Lane(data.race.track.lanes[i].index, data.race.track.lanes[i].distanceFromCenter);
        }


        for (var lane in this.lanes)  {

            this.routes[lane] = [];
            i = 0;
            var len = 0;
            var currentType = this.pieces[0].type;
            var currentAngle = this.pieces[0].angle;
            var currentPieces = [];

            while (i < this.pieces.length) {

                if (this.pieces[i].type != currentType || (this.pieces[i].type === "BEND"  && this.pieces[i].angle != currentAngle) || this.pieces.length === i + 1) {

                    var route = new Route();
                    route.type   = currentType;
                    route.angle  = currentAngle;
                    route.pieces = currentPieces;
                    route.alphaMax = 59; // TODO update
                    route.length = len;

                    this.routes[lane].push(route);

                    len = 0;
                    currentType = this.pieces[i].type;
                    currentAngle = this.pieces[i].angle;
                    currentPieces = [];
                }
                currentPieces.push(this.pieces[i].index);
                len += this.pieces[i].getPieceLength(this.lanes[lane]);
                i++;
            }

            // Combine first and last route if they are the same
            if (this.routes[lane][0].type === this.routes[lane][this.routes[lane].length - 1].type) {

                if (this.routes[lane][0].type === 'STRAIGHT') {
                    this.routes[lane][0].length += this.routes[lane][this.routes[lane].length - 1].length;
                    this.routes[lane][0].pieces = this.routes[lane][0].pieces.concat(this.routes[lane][this.routes[lane].length - 1].pieces);
                    this.routes[lane] = this.routes[lane].slice(0, this.routes[lane].length - 1);
                }

                if (this.routes[lane][0].type === 'BEND' &&
                    this.routes[lane][0].angle === this.routes[lane][this.routes[lane].length - 1].angle) {

                    this.routes[lane][0].length += this.routes[lane][this.routes[lane].length - 1].length;
                    this.routes[lane][0].pieces = this.routes[lane][0].pieces.concat(this.routes[lane][this.routes[lane].length - 1].pieces);
                    this.routes[lane] = this.routes[lane].slice(0, this.routes[lane].length - 1);

                }

            }

        }

    },

    getPastDistance: function(index, pieceDistance, lane, lap) {

        var distance = 0;
        for (var k = 0; k < lap; k++) {
            for (var i = 0; i < this.pieces.length; i++) {
                distance += this.pieces[i].getPieceLength(this.lanes[lane]);
            }
        }

        for (var i = 0; i < index; i++) {
            distance += this.pieces[i].getPieceLength(this.lanes[lane]);
        }

        return distance + pieceDistance;

    },

    updateRouteInterpolationFunction: function(cRoute, f, name) {

        for (var lane in this.lanes)  {
            for (var i = 0; i < this.routes[lane].length; i++) {
                if (this.routes[lane][i].type === cRoute.type) {
                    this.routes[lane][i][name] = f;
                }
            }
        }

    },

    updateVFunction: function(cRoute, f, name) {
        for (var lane in this.lanes)  {
            for (var i = 0; i < this.routes[lane].length; i++) {
                this.routes[lane][i][name] = f;
            }
        }

    },

    getCurrentRoute: function(pieceIndex) {
        for (var i = 0; i < this.routes[0].length; i++) {
            if (_.indexOf(this.routes[0][i].pieces, pieceIndex) >= 0 ) {
                return this.routes[0][i];
            }
        }

        throw new Error("Route with index " + pieceIndex + " was not found");
    }


});




module.exports.Track = Track;
